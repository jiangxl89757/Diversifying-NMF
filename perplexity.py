import numpy as np;

def cal_perplexity(U,V):
	s = 0
	D = np.dot(U,V)
	pw = D.sum(0)
	print pw.nonzero()
	pw = np.log(pw);
	return np.exp(pw.sum())


if __name__ == "__main__":
	file_path = "result/JXL_topic_100_iter100-U.txt"
	U = np.loadtxt(file_path,delimiter=',')
	file_path = "result/JXL_100-V.txt"
	V = np.loadtxt(file_path,delimiter=',')
	p = cal_perplexity(U,V)
	print p