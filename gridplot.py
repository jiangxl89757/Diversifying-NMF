import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from pylab import *
from sklearn import preprocessing


V = np.loadtxt("result/JXL_webotopic_10_iter_100_lmd1_0_lmd2_0.1_sparse_0-V.txt",delimiter=',')
U = np.loadtxt("result/JXL_webotopic_10_iter_100_lmd1_0_lmd2_0_sparse_0-V.txt",delimiter=',')
V = np.matrix(preprocessing.normalize(V,norm="l2"))
U = np.matrix(preprocessing.normalize(U,norm="l2"))
V = np.dot(V,V.T)
U = np.dot(U,U.T)

n,m = U.shape

cdict = {'red': ((0., 1, 1),
                 (0.05, 1, 1),
                 (0.11, 0, 0),
                 (0.66, 1, 1),
                 (0.89, 1, 1),
                 (1, 0.5, 0.5)),
         'green': ((0., 1, 1),
                   (0.05, 1, 1),
                   (0.11, 0, 0),
                   (0.375, 1, 1),
                   (0.64, 1, 1),
                   (0.91, 0, 0),
                   (1, 0, 0)),
         'blue': ((0., 1, 1),
                  (0.05, 1, 1),
                  (0.11, 1, 1),
                  (0.34, 1, 1),
                  (0.65, 0, 0),
                  (1, 0, 0))}
my_cmap = mpl.colors.LinearSegmentedColormap('my_colormap',cdict,256)

for i in range(m):
	U[i,i] = 1
fig = plt.figure(figsize=(6,12))
plt.title("Topics Similarity")
plt.subplot(211)
plt.title("(a)DNMF")
plt.imshow(V, cmap = my_cmap,origin='upper',interpolation='nearest')
plt.colorbar(shrink=1)
plt.subplot(212)
plt.title("(b)NMF")
mpl.cm.hot
plt.imshow(U, cmap = my_cmap, origin='upper',interpolation='nearest')
plt.colorbar(shrink=1)
plt.show()

# def f(x,y): return (1-x/2+x**5+y**3)*np.exp(-x**2-y**2)
#
# n = 10
# x = np.linspace(-3,3,4*n)
# y = np.linspace(-3,3,3*n)
# X,Y = np.meshgrid(x,y)
# imshow(f(X,Y)), show()