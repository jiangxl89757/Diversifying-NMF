import numpy as np;
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True
x=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
y = []
y.append([0.493,0.494,0.506,0.493,0.498,0.505,0.505,0.514,0.506,0.516,0.512])
y.append([0.51,0.511,0.525,0.513,0.513,0.521,0.522,0.53,0.521,0.528,0.52,])
y.append([0.486,0.488,0.507,0.493,0.492,0.502,0.501,0.511,0.502,0.506,0.501,])
constants = [
		{"color":"red","maker":"*","name":"Precision","linestyle":"-"},
		 {"color":"green","maker":"s","name":"Recall","linestyle":"-"},
		 {"color":"blue","maker":"^","name":"F-Measure","linestyle":"-"}]

def draw(host, x, y, x_titlet, y_title,constants):
    bar_width = 2.5
    opacity = 0.7
    # v_t = (x[1] - x[0])/4+2
    font = {'family': 'Arial',
            'color': 'black',
            'weight': 'normal',
            'size': 20,
            }

    min_y = min([min(item) for item in y])
    max_y =  max([max(item) for item in y])
    v_y = (max_y - min_y) /20

    host.set_xlim(x[0], x[-1])
    host.set_ylim(0.45,0.58)

    if x_titlet is not None:
        host.set_xlabel(x_titlet,fontdict=font)
    host.set_ylabel(y_title,fontdict=font)
    pts = []

    location_index = 1

    for kindex,yi in enumerate(y):
        index = kindex
        pts.append(host.plot(x, yi, alpha=opacity, label=constants[index]["name"],lw=1, linestyle=constants[index]["linestyle"],marker=constants[index]["maker"], markersize=10, color=constants[index]["color"])[0])

    host.legend(pts, [item["name"] for item in constants], loc=location_index, ncol=1, fancybox=True)

print y
fig, axes = plt.subplots()
draw(axes, x, y, "$\lambda_1$", "Precision/Recall/F-Measure",  constants)
fig.subplots_adjust(left=0.13, right=0.95, bottom=0.12,top = 0.95)
plt.rc('font', size=16, family='Arial')
plt.draw()
plt.show()