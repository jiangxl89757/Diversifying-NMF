import numpy as np;
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

def plotline(path):
	y=[];
	for line in path:
		M = np.loadtxt(line,delimiter=',');
		m,n = M.shape;
		p = M.sum(0);
		y.append(sorted(p,cmp = lambda x,y:cmp(y,x)));
	x = range(1,n+1);
	plt.plot((x,x,x,x),y)
	plt.show()

def draw(host, x, y, x_titlet, y_title,constants):
    bar_width = 2.5
    opacity = 0.7
    v_t = (x[1] - x[0])/4+2
    font = {'family': 'Arial',
            'color': 'black',
            'weight': 'normal',
            'size': 24,
            }

    min_y = min([min(item) for item in y])
    max_y =  max([max(item) for item in y])
    v_y = (max_y - min_y) /20

    host.set_xlim(x[0] - v_t, x[-1] + v_t)
    host.set_ylim(min_y - 2*v_y,max_y + 6*v_y)

    if x_titlet is not None:
        host.set_xlabel(x_titlet,fontdict=font)
    host.set_ylabel(y_title,fontdict=font)
    pts = []

    location_index = 1

    for kindex,yi in enumerate(y):
        index = kindex
        pts.append(host.plot(x, yi, alpha=opacity, label=constants[index]["name"],lw=3, linestyle=constants[index]["linestyle"], color=constants[index]["color"])[0])

    host.legend(pts, [item["name"] for item in constants], loc=location_index, ncol=1, fancybox=True)

if __name__ == "__main__":
	constants = [
		{"color":"purple","maker":"*","name":"no inverse proportion","linestyle":"-"}]
		# {"color":"green","maker":"s","name":"standard inverse proportion","linestyle":"-"},
		# {"color":"blue","maker":"^","name":"10 topics inverse proportion","linestyle":"-"},
		# {"color":"brown","maker":"^","name":"20 topics inverse proportion","linestyle":"-"},
		# {"color":"red","maker":"o","name":"100 topics inverse proportion","linestyle":"-"}]
	path = ["result/JXL_20newsgrouptopic_100_iter_100_lmd1_0_lmd2_0_sparse_0.01-U.txt"]
	        # "result/JXL1_topic_100_iter100-U.txt",
	        # "result/JXL2_topic_100_iter100-U.txt",
	        # "result/JXL3_topic_100_iter100-U.txt",
	        # "result/JXL4_topic_100_iter100-U.txt"];
	fig, axes = plt.subplots()
	y=[];
	for line in path:
		M = np.loadtxt(line,delimiter=',');
		m,n = M.shape;
		p = M.sum(0);
		print M.sum()
		# y.append(sorted(p,cmp = lambda x,y:cmp(y,x)));
		y.append(p)
	x = range(1,n+1);
	draw(axes, x, y, "Topic numbers", "",  constants)
	fig.subplots_adjust(left=0.13, right=0.95, bottom=0.12,top = 0.95)
	plt.rc('font', size=16, family='Arial')
	plt.draw()
	plt.show()